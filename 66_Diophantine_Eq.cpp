#include "problems.h"

void p066(int nArgs, char** args) {
    int maxD = 1000;
    if (nArgs > 1) maxD = std::stoi(args[1]);

    mpz_t maxX;
    mpz_t x, px, y, py;
    mpz_t tmp, res;
    mpz_inits(x, px, y, py, tmp, res, maxX, NULL);

    int DVal = 0;
    for (int D = 2; D <= maxD; D++) {
        int rtD = sqrt(D);
        if (rtD * rtD == D) continue;

        mpz_set_ui(x, rtD);
        mpz_set_ui(px, 1);
        mpz_set_ui(y, 1);
        mpz_set_ui(py, 0);
        mpz_set_ui(res, 0);

        int n = rtD, d = 1, intPt = 0, p = 0;
        while (mpz_cmp_ui(res, 1) != 0) {
            d = (D - n * n) / d;
            intPt = (rtD + n) / d;
            n = abs(n - intPt * d);

            mpz_set(tmp, x);
            mpz_mul_ui(x, x, intPt);
            mpz_add(x, x, px);
            mpz_set(px, tmp);

            mpz_set(tmp, y);
            mpz_mul_ui(y, y, intPt);
            mpz_add(y, y, py);
            mpz_set(py, tmp);
            
            mpz_pow_ui(res, x, 2);
            mpz_pow_ui(tmp, y, 2);
            mpz_submul_ui(res, tmp, D);

            p++;
        }

        //std::cout << D << ": " << x << ", " << y << " (" << p << ")\n";

        if (mpz_cmp(x, maxX) > 0) {
            mpz_set(maxX, x);
            DVal = D;
        }
    }

    std::cout << DVal << ": ";
    
    std::string str = mpz_get_str(NULL, 10, maxX);
    int p = str.length() % 3;
    for (int i = 0; i < str.length(); i++) {
        std::cout << str[i];
        if (i % 3 == 3 - p && i < str.length() - 1)
            std::cout << ",";
    }
    std::cout << std::endl;

    mpz_clears(x, px, y, py, tmp, res, maxX, NULL);
}