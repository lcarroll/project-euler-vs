/**
 * Finds the sum of the digits in the number 2^n
 * Problem description at https://projecteuler.net/problem=16
 *
 * Usage: MAIN 16 [NUMBER]
 * - NUMBER: The exponent
 */

#include "problems.h"

void p016(int nArgs, char** args) {
	int exp = 1000;
	if (nArgs > 1) exp = std::stoi(args[1]);

	mpz_t pow2;
	mpz_init(pow2);

	mpz_ui_pow_ui(pow2, 2, exp);
	std::string str = mpz_get_str(NULL, 10, pow2);

	int sum = 0;
	for (char dig : str)
		sum += dig - '0';

	std::cout << sum << std::endl;
}