/*
 * Finds the first n digits of a sum of numbers
 * Problem description at https://projecteuler.net/problem=13
 *
 * Usage: MAIN 13 [-f FILE] [-n NUMBER]
 */

#include <fstream>
#include "problems.h"

void p013(int nArgs, char** args) {
    std::string file = "../../../../Documents/Project Euler/input/p013_numbers.txt";
    int nDigs = 10;
    qst::checkInputs(nArgs, args, file, nDigs);

    mpz_t n, sum;
    mpz_inits(n, sum, NULL);

    std::ifstream in(file);
    std::string line;
    while (getline(in, line)) {
        mpz_set_str(n, &line[0], 10);
        mpz_add(sum, sum, n);
    }

    std::string sumStr = mpz_get_str(NULL, 10, sum);

    std::cout << sumStr.substr(0, (nDigs <= sumStr.length()) ? nDigs : sumStr.length()) << std::endl;
}