/**
* Takes a problem number and command-line arguments and passes them to correct function
* 
* Usage: PROGRAM [NUMBER] [ARGS]
* - NUMBER: The problem number
* - ARGS: Command-line arguments to pass to the problem-solving function
*/

#include "problems.h"

int main(int argc, char** argv) {
	int probNum = 0, nArgs = 1;
	char** args = argv;

	if (argc == 1) {
		std::cout << "Please enter a problem number: ";
		std::cin >> probNum;
	} else {
		probNum = std::stoi(argv[1]);
		nArgs = argc - 1;
		args = &argv[1];
	}

	switch (probNum) {
		case 13:
			p013(nArgs, args);
			break;
		case 16:
			p016(nArgs, args);
			break;
		case 20:
			p020(nArgs, args);
			break;
		case 25:
			p025(nArgs, args);
			break;
		case 29:
			p029(nArgs, args);
			break;
		case 66:
			p066(nArgs, args);
			break;
		default:
			std::cout << "The solution for problem " << probNum;
			std::cout << " has not been implemented in this project\n";
	}
}