/**
 * Finds the index of the first term in the Fibonacci sequence with n digits
 * Problem description at https://projecteuler.net/problem=25
 *
 * Usage: MAIN 25 [NUMBER]
 * - NUMBER: The desired number of digits
 */

#include "problems.h"

void p025(int nArgs, char** args) {
	int nDigs = (nArgs > 1) ? std::stoi(args[1]) : 1000;

	mpz_t f1, f2;
	mpz_init_set_ui(f1, 1);
	mpz_init_set_ui(f2, 2);

	int n = 3;
	while (mpz_sizeinbase(f2, 10) < nDigs) {
		mpz_add(f1, f1, f2);
		mpz_swap(f1, f2);
		n++;
	}

	std::cout << n + 1 << std::endl;
}