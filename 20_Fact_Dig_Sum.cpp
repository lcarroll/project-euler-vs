/**
 * Finds the sum of the digits of n!
 * Problem description at https://projecteuler.net/problem=20
 *
 * Usage: MAIN 20 [NUMBER]
 * - NUMBER: The factorial number
 */

#include "problems.h"

void p020(int nArgs, char** args) {
	int n = (nArgs > 1) ? std::stoi(args[1]) : 100;

	mpz_t fact;
	mpz_init(fact);

	mpz_set_ui(fact, 2);
	for (int i = 3; i <= n; i++) 
		mpz_mul_ui(fact, fact, i);

	std::string str = mpz_get_str(NULL, 10, fact);

	int sum = 0;
	for (char dig : str)
		sum += dig - '0';

	std::cout << sum << std::endl;
}