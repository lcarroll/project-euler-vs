#pragma once
#include <iostream>
#include <string>
#include <mpirxx.h>
#include "../../../../Documents/project-euler/helper.hpp"

void p013(int, char**);
void p016(int, char**);
void p020(int, char**);
void p025(int, char**);
void p029(int, char**);
void p066(int, char**);