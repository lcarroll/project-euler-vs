/**
 * Finds the number of distinct terms in the sorted sequence made by taking a^b with 2 <= a, b <= n
 * Problem description at https://projecteuler.net/problem=29
 *
 * Usage: MAIN 29 [NUMBER]
 * - NUMBER: The maximum value of a and b
 * 
 * Notes:
 * - Can take advantage of sets not allowing duplicates
 */

#include <unordered_set>
#include "problems.h"

void p029(int nArgs, char** args) {
	int min = 2;
	int max = (nArgs > 1) ? std::stoul(args[1]) : 100;

	mpz_t power;
	mpz_init(power);

	std::unordered_set<std::string> powers;
	for (int a = min; a <= max; a++) {
		for (int b = 2; b <= max; b++) {
			mpz_ui_pow_ui(power, a, b);
			powers.emplace(mpz_get_str(NULL, 10, power));
		}
	}

	std::cout << powers.size() << std::endl;
}